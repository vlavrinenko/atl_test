import re

import bs4
import requests

from flask_restful import Resource


class MessageInfo(Resource):
    """RESTful service providing info about chat message."""

    def get(self, message):
        result = {
            'mentions': list(self.get_mentions(message)),
            'emoticons': list(self.get_emoticons(message)),
            'links': list(map(self.full_link, self.get_links(message))),
        }
        # Drop empty keys
        return {key: value for key, value in result.items() if value}

    @staticmethod
    def get_mentions(message):
        """Generate all @mentions from a message."""
        for match in re.finditer(r'@(\w+)', message, re.I):
            yield match[1]

    @staticmethod
    def get_emoticons(message):
        """Generate all (emoticons) from message."""
        for match in re.finditer(r'\((\w{1,15})\)', message, re.I):
            yield match[1]

    @staticmethod
    def get_links(message):
        """Generate all links from message."""
        # Regex comes from RegexBuddy's library; it's definitely not perfect.
        regex = (
            r'\bhttps?://[-A-Z0-9+&@#/%?=~_|!:,.;]*[A-Z0-9+&@#/%=~_|]'
        )
        for match in re.finditer(regex, message, re.I):
            url = match[0]
            yield url

    @staticmethod
    def full_link(url):
        """Get page title for url if possible; return description dict."""
        try:
            return {
                'url': url,
                'title': MessageInfo.get_page_title(url),
            }
        except:
            return {'url': url}

    @staticmethod
    def get_page_title(url):
        content = requests.get(url).text
        soup = bs4.BeautifulSoup(content, 'html.parser')
        return soup.title.string
