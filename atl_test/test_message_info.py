from unittest import TestCase
from unittest.mock import Mock, patch

from .message_info import MessageInfo


class MentionTestCase(TestCase):

    def test_positive(self):
        mentions = list(MessageInfo.get_mentions('@foo-@bar'))
        self.assertEqual(mentions, ['foo', 'bar'])

    def test_empty(self):
        """Assure no empty mentions are returned."""
        mentions = list(MessageInfo.get_mentions('@.@'))
        self.assertEqual(mentions, [])


class EmoticonTestCase(TestCase):

    def test_positive(self):
        emoticons = list(MessageInfo.get_emoticons('(foo)-(bar)'))
        self.assertEqual(emoticons, ['foo', 'bar'])

    def test_non_alpanum(self):
        """Non-alphanumeric chars are not allowed in emoticons."""
        emoticons = list(MessageInfo.get_emoticons('(foo-bar)'))
        self.assertEqual(emoticons, [])

    def test_long(self):
        """Emoticons shouldn't be longer than 15 chars."""
        emoticons = list(MessageInfo.get_emoticons(
            '(foo1234567890bar)'
        ))
        self.assertEqual(emoticons, [])


class LinkTestCase(TestCase):

    def test_simple(self):
        """Check simple URL and trailing punctuation removal."""
        links = list(MessageInfo.get_links('! http://google.com/?foo%20bar.'))
        self.assertEqual(links, ['http://google.com/?foo%20bar'])


class MessageInfoTestCase(TestCase):
    """Integration tests for MessageInfo class."""

    def test_emoticons_only(self):
        result = MessageInfo().get('Good morning! (megusta) (coffee)')
        self.assertEqual(result, {
            'emoticons': [
                'megusta',
                'coffee',
            ]
        })

    @patch('requests.get', Mock(return_value=Mock(
        text='<html><head><title>TEST</title></head></html>'
    )))
    def test_all(self):
        result = MessageInfo().get(
            '@bob @john (success) such a cool feature; '
            'https://twitter.com/jdorfman/status/430511497475670016'
        )
        self.assertEqual(result, {
            'mentions': [
                'bob',
                'john',
            ],
            'emoticons': [
                'success',
            ],
            'links': [
                {
                    'url': (
                        'https://twitter.com/jdorfman/status/'
                        '430511497475670016'
                    ),
                    'title': 'TEST',
                }
            ],
        })
