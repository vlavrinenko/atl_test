from flask import Flask
from flask_restful import Api

from atl_test.message_info import MessageInfo


app = Flask(__name__)
api = Api(app)
api.add_resource(MessageInfo, '/message_info/<path:message>')

if __name__ == '__main__':
    app.run(debug=True)
