# Atlassian test assignement


Using flask and flask-restful for RESTful API; Pico (https://github.com/fergalwalsh/pico) could also be used for this specific task, but it doesn't support many REST features that could become required later.

## To test:
    make run

    wget --quiet -O - "http://127.0.0.1:5000/message_info/@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
