PYTHON = /usr/bin/env python3


run:
	$(PYTHON) app.py


test:
	$(PYTHON) -m unittest
